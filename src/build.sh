#!/bin/bash

# game2048 build script
# FreePascal Project

source libbuild.sh

# -- CONSTANTS --


# action numbers

build_debug=1
build_release=2
clean_src=3

# file names

main_src_name="main.pas"
prog_name="game2048"

# other

check_err_pause=2  # pause when compile failed

# -- FUNCTIONS --

write_title() {
  echo -e "\n\e[36m---------------------"
  echo -e "GAME2048 build script"
  echo -e "---------------------\e[0m\n"
}

write_action_select() {
  echo -e "Please choose an action\n"
  echo -e "\e[36m[ 1 ]\e[0m - build \e[31mDEBUG\e[0m version"
  echo -e "\e[36m[ 2 ]\e[0m - build \e[32mRELEASE\e[0m version"
  echo -e "\e[36m[ 3 ]\e[0m - \e[35mCLEAR\e[0m source and \e[33m/BIN/*\e[0m directories\n"
  echo -e "\e[36m[ Any key ]\e[0m - exit\n"
  echo -e "Your choice : "
}

purge_dir() {
  rm $1/*.ppu $1/*.o $1/*~ $1/game2048 $1/main 2>/dev/null
}


#---------------------------#
#---------------------------#
#        MAIN SCRIPT        #
#---------------------------#
#---------------------------#


write_title
write_action_select
read -n 1 confirm

case $confirm in

# - debug build

  $build_debug)
  create_bin_dirs
  fpc $fpc_debug_key -o$debug_bin_dir/$prog_name $main_src_name
  check_compile_results $?
  ;;

# - release build

  $build_release)
  create_bin_dirs
  fpc $fpc_release_key -o$release_bin_dir/$prog_name $main_src_name
  check_compile_results $?
  ;;

# - deleting compile results

  $clean_src)
  echo -e "\nFollowing files will be deleted:\e[1;33m"
  echo -n $(ls *.o *.ppu *~ main 2>/dev/null)

  echo -n $(ls $bin_dir/*.o $bin_dir/*.ppu $bin_dir/*~ $bin_dir/game2048 2>/dev/null)

  echo -n $(ls $debug_bin_dir/*.o $debug_bin_dir/*.ppu 2>/dev/null)
  echo -n $(ls $debug_bin_dir/*~ $debug_bin_dir/game2048 2>/dev/null)

  echo -n $(ls $release_bin_dir/*.o $release_bin_dir/*.ppu 2>/dev/null)
  echo -n $(ls $release_bin_dir/*~ $release_bin_dir/game2048 2>/dev/null)

  purge_dir $src_dir
  purge_dir $bin_dir
  purge_dir $debug_bin_dir
  purge_dir $release_bin_dir
  echo -e "\n\e[32mR E A D Y !\e[0m"
  ;;

# - exit without action

  *)
    echo "Exit..."
esac
exit 0
