{
  --- CRT_BASE.PAS ---

  Screen coordinates with CRT unit
  for *NIX terminals

  (c) Alexey Sorokin, 2018
}


Unit CRT_Base;


INTERFACE


USES
  CRT;

TYPE

Point=record
  Col,Row:Word
end;


FrameSet=record
  Angle,HLine,VLine:Char
end;

CONST

{ CRT unit keyboard values }

kbdUp=#72;
kbdDown=#80;
kbdLeft=#75;
kbdRight=#77;

kbdENTER=#13;
kbdTAB=#9;
kbdBackspace=#8;
kbdSpace=#32;
kbdESC=#27;

{symbols for Frame}

frameClear:FrameSet=
(Angle:' ';HLine:' ';VLine:' ');

frameLine:FrameSet=
(Angle:'+';HLine:'-';VLine:'|');

{ PROCEDURES AND FUNCTIONS}

Function MiddleCol:Word;

Function MiddleRow:Word;

Function GetScreenMiddle:Point;

Function GetMidRectLeftTop(RectWidth,RectHeight:Word):Point;

Procedure ClearLine(Num:Word);

Procedure ClearRect(LeftTop:Point;ColNum,RowNum:Word);

Function ClearRectInMid(ColNum,RowNum:Word):Point;

Procedure CursorOut;

Procedure DrawFrame2Angle(TopAngle,BottomAngle:Point;Frame:FrameSet);

Procedure ResetTerminal;


IMPLEMENTATION


Function MiddleCol:Word;
begin
  MiddleCol:=ScreenWidth div 2;
end;

{ --- }

Function MiddleRow:Word;
begin
  MiddleRow:=ScreenHeight div 2;
end;

{ --- }

Function GetScreenMiddle:Point;
var
  A:Point;
begin
  A.Col:=MiddleCol;
  A.Row:=MiddleRow;
  GetScreenMiddle:=A;
end;

{ --- }

Function GetMidRectLeftTop(RectWidth,RectHeight:Word):Point;
var
  A,M:Point;
begin
  M:=GetScreenMiddle;
  A.Col := M.Col - ( RectWidth  div 2 );
  A.Row := M.Row - ( RectHeight div 2 );
  GetMidRectLeftTop:=A;
end;

{ --- }

Procedure ClearLine(Num:Word);
begin
  While Num>0 do
  begin
    Write(#32);
    dec(Num);
  end;
end;

{ --- }

Procedure ClearRect(LeftTop:Point;ColNum,RowNum:Word);
var
  i:Word;
begin
  for i:=0 to RowNum-1 do
  begin
    GotoXY(LeftTop.Col,LeftTop.Row+i);
    ClearLine(ColNum);
  end;
end;

{ --- }

Function ClearRectInMid(ColNum,RowNum:Word):Point;
var
  MidLeftTop:Point;
begin
  MidLeftTop:=GetMidRectLeftTop(ColNum,RowNum);
  ClearRect(MidLeftTop,ColNum,RowNum);
  ClearRectInMid:=MidLeftTop;
end;

{ --- }

Procedure CursorOut;
begin
  GotoXY(ScreenWidth,1);
end;


{ --- }

Procedure DrawFrame2Angle(TopAngle,BottomAngle:Point;Frame:FrameSet);
var
  i:Word;
begin
  GotoXY(TopAngle.Col,TopAngle.Row);
  Write(Frame.Angle);
  for i:=TopAngle.Col+1 to BottomAngle.Col-1 do
  begin
    GotoXY(i,TopAngle.Row);
    Write(Frame.HLine);
    GotoXY(i,BottomAngle.Row);
    Write(Frame.HLine);
  end;
  GotoXY(TopAngle.Col,BottomAngle.Row);
  Write(Frame.Angle);
  GotoXY(BottomAngle.Col,TopAngle.Row);
  Write(Frame.Angle);
  for i:=TopAngle.Row+1 to BottomAngle.Row-1 do
  begin
    GotoXY(TopAngle.Col,i);
    Write(Frame.VLine);
    GotoXY(BottomAngle.Col,i);
    Write(Frame.VLine);
  end;
  GotoXY(BottomAngle.Col,BottomAngle.Row);
  Write(Frame.Angle);
end;

{ --- }

Procedure ResetTerminal;
begin
  Write(#27,'[0m');
end;

{ --- *** --- }

END.