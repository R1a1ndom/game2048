#  ---  LIBBUILD.SH ---
# little library for building shell-scripts

# directories

src_dir=.
bin_dir=../bin
debug_bin_dir=../bin/debug
release_bin_dir=../bin/release

# compiler keys

fpc_debug_key="-g -dDEBUG" # adding debug info and DEBUG define
fpc_release_key="-o3"      # maximum optimization

# other

check_err_pause=1

# -----------------
# --- functions ---
# -----------------

create_bin_dirs() {
  if [ -d $bin_dir ]; then
    :
  else
    mkdir $bin_dir
    echo -e "\e[32mCreate \e[1;33m$bin_dir\e[0;32m directory"
  fi

  if [ -d $debug_bin_dir ]; then
    :
  else
    mkdir $debug_bin_dir
    echo -e "Create \e[1;33m$debug_bin_dir\e[0;32m directory"
  fi

  if [ -d $release_bin_dir ]; then
    :
  else
    mkdir $release_bin_dir
    echo -e "Create \e[1;33m$release_bin_dir\e[0;32m directory"
  fi

  echo -e "\e[0m"
}

check_compile_results() {
  if [ $1 != 0 ]; then
    echo -e "\e[1;31mCOMPILE ERRORS...\e[0m\n"
    read -s -n 1 -t $check_err_pause -p "Press [any key] ..."
    echo
    return 1
  else
    echo -e "\e[1;32mCOMPILE SUCCESSFULLY !\e[0m"
    return 0
  fi
}