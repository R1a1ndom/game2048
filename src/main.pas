{  --- MAIN2048.PAS ---

Puzzle game 2048
text-mode console game
written on Free Pascal

}

{ $ifdef DEBUG}
{ $else}
{ $endif}


Program Game2048;
Uses
  CRT,CRT_Base,Unit2048,TUI2048,Shift2048;

{ PROCEDURES and FUNCTIONS }

Procedure GameInit(var FieldLeftTop:Point);
begin
  ShowTitleScreen;
  repeat
  until KeyPressed;
  Randomize;
  FieldLeftTop:=GetMidRectLeftTop(FieldWidth,FieldHeight);
end;

{ --- }

procedure StartNewGame
    (var A:GameField;var CurrentScore:LongWord;var EmptyCells:Word
{$ifdef DEBUG}
     ; var IterationsNum:Word);
{$else}
     );
{$endif}
var
  i:Word;
begin
  ClearField(A);
  for i:=0 to 1 do
{$ifdef DEBUG}
    AddNewNumber(A,IterationsNum);
{$else}
    AddNewNumber(A);
{$endif}
  CurrentScore:=0;
  EmptyCells:=MaxCells-2;
end;

{ --- }

Procedure RefreshField(var A:GameField;LeftTop:Point);
begin
  DrawGrid;
  DrawAllField(LeftTop,A);
  CursorOut;
end;

{ --- }

Function CountEmptyCells(var A:GameField):Word;
var
  tmp,i,j:Word;
begin
  tmp:=0;
  for i:=0 to FieldSize do
    for j:=0 to FieldSize do
      if A[i,j].Value=0 then
        inc(tmp);
  CountEmptyCells:=tmp;
end;

{ --- }

Procedure ShiftField
  (sym:Char;var A:GameField;LeftTop:Point;
   var Result:Word;var State:ProgramState);
begin
  case sym of
    kbdUp:
      ShiftFieldUp(A,LeftTop,Result);
    kbdDown:
      ShiftFieldDown(A,LeftTop,Result);
    kbdLeft:
      ShiftFieldLeft(A,LeftTop,Result);
    kbdRight:
      ShiftFieldRight(A,LeftTop,Result);
    end;
  State:=stContinue;
end;


{ MAIN PROGRAM VARIABLES }

VAR
  MainField    : GameField;
  MainLeftTop  : Point;

  LastMove     : UndoData;

  State        : ProgramState;
  MoveCancelled: Boolean;

  EmptyCells   : Word;
  ShiftResult  : Word;

  CurrentScore,
  HighScore    : LongWord;

  ch           : Char;
{$ifdef DEBUG}
  Iterations   : Word;   { iterations of new number generation }
{$endif}


{ --- --- --- --- ---
     MAIN  PROGRAM
  --- --- --- --- ---}

BEGIN

  GameInit(MainLeftTop);

  ResetTerminal;
  ClrScr;
{$ifdef DEBUG}
  StartNewGame(MainField,CurrentScore,EmptyCells,Iterations);
{$else}
  StartNewGame(MainField,CurrentScore,EmptyCells);
{$endif}

  State:=stContinue;
  MoveCancelled:=false;
  CurrentScore:=0;
  HighScore:=0;
  ShiftResult:=0;

  ClearMenuArea;
  ClearUndoData(LastMove);

{$ifdef DEBUG}
  ShowDebugInfo(Iterations,EmptyCells);
{$else}
  ShowHintLine(hintline_Main);
{$endif}
  RefreshField(MainField,MainLeftTop);
  CursorOut;

  repeat
    ch:=ReadKey;
    case ch of    { ARROWS and other keys }

      kbdUp,kbdDown,kbdLeft,kbdRight:
      begin
        LastMove.Field:=MainField;
        LastMove.Score:=CurrentScore;
        LastMove.HiScore:=HighScore;
        MoveCancelled:=false;
        ShiftField(ch,MainField,MainLeftTop,ShiftResult,State);
        ResetMove(MainField);
        WriteScore(MainLeftTop,CurrentScore,HighScore);
        if State <> stWinner then
        begin
          CurrentScore := CurrentScore + ShiftResult;
          EmptyCells:=CountEmptyCells(MainField);
          if EmptyCells=0 then
          begin
            if NoMoves(MainField) then
              State:=stNoMoves
          end
          else begin
{$ifdef DEBUG}
            AddNewNumber(MainField,Iterations);
            ShowDebugInfo(Iterations,EmptyCells);
{$else}
            AddNewNumber(MainField);
{$endif}
            State:=stContinueRedraw;
          end;
        end;
      end;

      kbdBackspace:begin  { UNDO last move }
        if not MoveCancelled then
        begin
          with LastMove do
          begin
            MainField:=Field;
            CurrentScore:=Score;
            HighScore:=HiScore;
          end;
          DrawAllField(MainLeftTop,MainField);
          MoveCancelled:=true;
          State:=stContinue;
        end;
      end;

      kbdESC:
        State:=GameMenu;
    end;

    case State of   { game menu actions and states after move}
      stNewGame:begin
        State:=NewGameBox;
        State:=stContinueRedraw;
      end;

      stShowAbout:begin

      end;

      stNoMoves: begin

      end;

      stWinner: begin

      end;

    end;

    if State=stContinueRedraw then
    begin
      RefreshField(MainField,MainLeftTop);
      State:=stContinue;
    end;

  Until State=stQuitGame;
  ResetTerminal;
END.
