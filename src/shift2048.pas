{  ---  SHIFT2048.PAS  ---
  text-mode console puzzle game

  shifting game field unit

  build with BUILD_FOR_TEST variable - test unit build
}

UNIT Shift2048;


INTERFACE

USES
  CRT_Base,Unit2048;


const
{$ifdef DEBUG}
  DrawPause=100;
{$else}
  DrawPause=50;
{$endif}

Function NoMoves(var A:GameField):Boolean;

procedure ResetMove(var A:GameField);

{$ifdef BUILD_FOR_TEST}

procedure ConsoleFieldOut(var A:GameField);

{$endif}

{ shifts of game field }

procedure ShiftFieldRight(var A:GameField;LeftTop:Point;var Result:Word);

procedure ShiftFieldLeft(var A:GameField;LeftTop:Point;var Result:Word);

procedure ShiftFieldUp(var A:GameField;LeftTop:Point;var Result:Word);

procedure ShiftFieldDown(var A:GameField;LeftTop:Point;var Result:Word);


IMPLEMENTATION


USES
  CRT;

Function NoMoves(var A:GameField):Boolean;
var
  i,j:Word;
  MoveFound:boolean;
begin
  MoveFound:=false;
  for i:=0 to FieldSize do
  begin
    for j:=0 to FieldSize-1 do
    begin
      if A[i,j].Value=A[i,j+1].value then
      begin
        MoveFound:=true;
        break;
      end;
      if A[j,i].Value=A[j+1,i].value then
      begin
        MoveFound:=true;
        break;
      end;
    end;
    if MoveFound then
      break;
  end;
  NoMoves:= not MoveFound;
end;

{---}

procedure ResetMove(var A:GameField);
var
  i,j:Word;
begin
  for i:=0 to FieldSize do
    for j:=0 to FieldSize do
      A[i,j].Merged:=false;
end;

{ --- }

function DoubleCell(var A:GameField; V,H:Word):Word;
begin
  A[V,H].value:=A[V,H].value*2;
  inc(A[V,H].Palette);
  A[V,H].Merged:=true;
  DoubleCell:=A[V,H].value;
end;

{ --- }

{$ifdef BUILD_FOR_TEST}

{ procedure for T_SHIFT test }

procedure ConsoleFieldOut(var A:GameField);
var
  pos_h,pos_v:Word;
begin
  for pos_v:=0 to FieldSize do
  begin
    Write('|');
    for pos_h:=0 to FieldSize do
    begin
      Write(A[pos_v,pos_h].Value:3);
      if A[pos_v,pos_h].Merged then
        Write('m |')
      else
        Write('* |');
    end;
    WriteLn;
  end;
  WriteLn;
end;

{$endif}

{ -----------------------------
  ---                       ---
  ---  game field shifts :  ---
  ---                       ---
  -----------------------------

    RIGHT -->
}

Procedure ShiftFieldRight(var A:GameField;LeftTop:Point;var Result:Word);

{ try to merge current and next cells }

  Function TryToMergeCellsRight(var A:GameField; V,H:Word):Word;
  begin
    if (not A[V,H+1].Merged) and (A[V,H+1].Value=A[V,H].Value) then
    begin
      ResetCell(A,V,H);
      TryToMergeCellsRight:=DoubleCell(A,V,H+1);
    end else
      TryToMergeCellsRight:=0;
  end;

{ push current cell right }

  function PushCellsRight(var A:GameField; V,H : Word):Word;
  begin
    while H<FieldSize do
    begin
      if A[V,H+1].Value=0 then
      begin
        A[V,H+1]:=A[V,H];
        ResetCell(A,V,H);
        inc(H);
      end else
        Break;
    end;
    if H<FieldSize then
      PushCellsRight:=TryToMergeCellsRight(A,V,H)
    else
      PushCellsRight:=0;
  end;

{ merge cell on current place with cell on prevous place }

  function ShiftOneCellRight(var A:GameField;V,H:Word):Word;
  var
    MergeResult:Word;
  begin
    Result:=0;
    if A[V,H].Value=0 then
    begin
      if A[V,H-1].Value<>0 then
        Result:=Result+PushCellsRight(A,V,H-1);
    end else
    begin
      MergeResult := TryToMergeCellsRight(A,V,H-1);
      if MergeResult = 0 then
        Result:=Result  + PushCellsRight(A,V,H)
      else
        Result:=Result + MergeResult;
    end;
    ShiftOneCellRight:=Result;
  end;

var
  pos_h,pos_v:Word;

begin  { ShiftFieldRight }

  Result:=0;
  For pos_h:=FieldSize downto 1 do
  begin
    for pos_v:=0 to FieldSize do
{$ifdef BUILD_FOR_TEST}
    begin
      Result:=Result + ShiftOneCellRight(A,pos_v,pos_h);
      ConsoleFieldOut(A);
      Delay(DrawPause);
    end;
{$else}
      Result:=Result + ShiftOneCellRight(A,pos_v,pos_h);
    DrawAllField(LeftTop,A);
    Delay(DrawPause);
{$endif}
  end;

end;   { ShiftFieldRight }


{
    LEFT  <--
}

Procedure ShiftFieldLeft(var A:GameField;LeftTop:Point;var Result:Word);

{ try to merge current and next cells }

  Function TryToMergeCellsLeft(var A:GameField; V,H:Word):Word;
  begin
    if (not A[V,H-1].Merged) and (A[V,H-1].Value=A[V,H].Value) then
    begin
      ResetCell(A,V,H);
      TryToMergeCellsLeft:=DoubleCell(A,V,H-1);
    end else
      TryToMergeCellsLeft:=0;
  end;

{ push current cell right }

  function PushCellsLeft(var A:GameField; V,H : Word):Word;
  begin
    while H>0 do
    begin
      if A[V,H-1].Value=0 then
      begin
        A[V,H-1]:=A[V,H];
        ResetCell(A,V,H);
        dec(H);
      end else
        Break;
    end;
    if H>0 then
      PushCellsLeft:=TryToMergeCellsLeft(A,V,H)
    else
      PushCellsLeft:=0;
  end;

{ merge cell on current place with cell on prevous place }

  function ShiftOneCellLeft(var A:GameField;V,H:Word):Word;
  var
    MergeResult:Word;
  begin
    Result:=0;
    if A[V,H].Value=0 then
    begin
      if A[V,H+1].Value<>0 then
        Result:=Result+PushCellsLeft(A,V,H+1);
    end else
    begin
      MergeResult := TryToMergeCellsLeft(A,V,H+1);
      if MergeResult = 0 then
        Result:=Result  + PushCellsLeft(A,V,H)
      else
        Result:=Result + MergeResult;
    end;
    ShiftOneCellLeft:=Result;
  end;

var
  pos_h,pos_v:Word;
begin
  Result:=0;
  For pos_h:=0 to FieldSize-1 do
  begin
    for pos_v:=0 to FieldSize do
{$ifdef BUILD_FOR_TEST}
    begin
      Result:=Result + ShiftOneCellLeft(A,pos_v,pos_h);
      ConsoleFieldOut(A);
      Delay(DrawPause);
    end;
{$else}
      Result:=Result + ShiftOneCellLeft(A,pos_v,pos_h);
    DrawAllField(LeftTop,A);
    Delay(DrawPause);
{$endif}
  end;
end;

{      ^
   UP  |
}

Procedure ShiftFieldUp(var A:GameField;LeftTop:Point;var Result:Word);

{ try to merge current and next cells }

  Function TryToMergeCellsUp(var A:GameField; V,H:Word):Word;
  begin
    if (not A[V-1,H].Merged) and (A[V-1,H].Value=A[V,H].Value) then
    begin
      ResetCell(A,V,H);
      TryToMergeCellsUp:=DoubleCell(A,V-1,H);
    end else
      TryToMergeCellsUp:=0;
  end;

{ push current cell right }

  function PushCellsUp(var A:GameField; V,H : Word):Word;
  begin
    while V>0 do
    begin
      if A[V-1,H].Value=0 then
      begin
        A[V-1,H]:=A[V,H];
        ResetCell(A,V,H);
        dec(V);
      end else
        Break;
    end;
    if V>0 then
      PushCellsUp:=TryToMergeCellsUp(A,V,H)
    else
      PushCellsUp:=0;
  end;  
  
{ merge cell on current place with cell on prevous place }

  function ShiftOneCellUp(var A:GameField;V,H:Word):Word;
  var
    MergeResult:Word;
  begin
    Result:=0;
    if A[V,H].Value=0 then
    begin
      if A[V+1,H].Value<>0 then
        Result:=Result+PushCellsUp(A,V+1,H);
    end else
    begin
      MergeResult := TryToMergeCellsUp(A,V+1,H);
      if MergeResult = 0 then
        Result:=Result  + PushCellsUp(A,V,H)
      else
        Result:=Result + MergeResult;
    end;
    ShiftOneCellUp:=Result;
  end;

var
  pos_h,pos_v:Word;
begin
  Result:=0;
  For pos_v:=0 to FieldSize-1 do
  begin
    for pos_h:=0 to FieldSize do
{$ifdef BUILD_FOR_TEST}
    begin
      Result:=Result + ShiftOneCellUp(A,pos_v,pos_h);
      ConsoleFieldOut(A);
      Delay(DrawPause);
    end;
{$else}
      Result:=Result + ShiftOneCellUp(A,pos_v,pos_h);
    DrawAllField(LeftTop,A);
    Delay(DrawPause);
{$endif}
  end;
end;

{        |
   DOWN  V
}

Procedure ShiftFieldDown(var A:GameField;LeftTop:Point;var Result:Word);

{ try to merge current and next cells }

  Function TryToMergeCellsDown(var A:GameField; V,H:Word):Word;
  begin
    if (not A[V+1,H].Merged) and (A[V+1,H].Value=A[V,H].Value) then
    begin
      ResetCell(A,V,H);
      TryToMergeCellsDown:=DoubleCell(A,V+1,H);
    end else
      TryToMergeCellsDown:=0;
  end;

{ push current cell right }

  function PushCellsDown(var A:GameField; V,H : Word):Word;
  begin
    while V<FieldSize do
    begin
      if A[V+1,H].Value=0 then
      begin
        A[V+1,H]:=A[V,H];
        ResetCell(A,V,H);
        inc(V);
      end else
        break;
    end;
    if V<FieldSize then
      PushCellsDown:=TryToMergeCellsDown(A,V,H)
    else
      PushCellsDown:=0;
  end;

{ merge cell on current place with cell on prevous place }

  function ShiftOneCellDown(var A:GameField;V,H:Word):Word;
  var
    MergeResult:Word;
  begin
    Result:=0;
    if A[V,H].Value=0 then
    begin
      if A[V-1,H].Value<>0 then
        Result:=Result+PushCellsDown(A,V-1,H);
    end else
    begin
      MergeResult := TryToMergeCellsDown(A,V-1,H);
      if MergeResult = 0 then
        Result:=Result  + PushCellsDown(A,V,H)
      else
        Result:=Result + MergeResult;
    end;
    ShiftOneCellDown:=Result;
  end;

var
  pos_h,pos_v:Word;
begin
  Result:=0;
  For pos_v:=FieldSize downto 1 do
  begin
    for pos_h:=0 to FieldSize do
{$ifdef BUILD_FOR_TEST}
    begin
      Result:=Result + ShiftOneCellDown(A,pos_v,pos_h);
      ConsoleFieldOut(A);
      Delay(DrawPause);
    end;
{$else}
      Result:=Result + ShiftOneCellDown(A,pos_v,pos_h);
    DrawAllField(LeftTop,A);
    Delay(DrawPause);
{$endif}
  end;
end;

{ --- === --- === --- }

END.