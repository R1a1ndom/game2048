{ --- T_SHIFT.PAS ---
game 2048 unit SHIFT2048.PAS test
}

Program T_Shift;
Uses
  CRT,CRT_Base,Unit2048,Shift2048;

CONST
  TestQuantity  = 4;
  ViewPause     = 3000;

TYPE
  RightPattern = array[0..FieldSize,0..FieldSize] of Word;
  TestResultList = array[1..TestQuantity] of Boolean;

CONST

  TestFieldOne : GameField=

(((Value:2 ; Palette:1 ; Merged:false),(Value:2 ; Palette:2 ; Merged:false),
  (Value:2 ; Palette:1 ; Merged:false),(Value:4 ; Palette:1 ; Merged:false)),

 ((Value:0 ; Palette:0 ; Merged:false),(Value:4 ; Palette:2 ; Merged:false),
  (Value:4 ; Palette:2 ; Merged:false),(Value:4 ; Palette:2 ; Merged:false)),

 ((Value:8 ; Palette:3 ; Merged:false),(Value:0 ; Palette:3 ; Merged:false),
  (Value:0 ; Palette:3 ; Merged:false),(Value:8 ; Palette:3 ; Merged:false)),

 ((Value:16; Palette:4 ; Merged:false),(Value:0; Palette:4 ; Merged:false),
  (Value:16; Palette:4 ; Merged:false),(Value:0; Palette:4 ; Merged:false)));

  TestFieldTwo : GameField=

(((Value:4 ; Palette:1 ; Merged:false),(Value:2 ; Palette:2 ; Merged:false),
  (Value:2 ; Palette:1 ; Merged:false),(Value:2 ; Palette:1 ; Merged:false)),

 ((Value:4 ; Palette:0 ; Merged:false),(Value:4 ; Palette:2 ; Merged:false),
  (Value:4 ; Palette:2 ; Merged:false),(Value:0 ; Palette:0 ; Merged:false)),

 ((Value:8 ; Palette:3 ; Merged:false),(Value:0 ; Palette:3 ; Merged:false),
  (Value:0 ; Palette:3 ; Merged:false),(Value:8 ; Palette:3 ; Merged:false)),

 ((Value:0 ; Palette:4 ; Merged:false),(Value:16; Palette:4 ; Merged:false),
  (Value:0 ; Palette:4 ; Merged:false),(Value:16; Palette:4 ; Merged:false)));

 TestFieldThree : GameField=

(((Value:4 ; Palette:1 ; Merged:false),(Value:4 ; Palette:2 ; Merged:false),
  (Value:8 ; Palette:1 ; Merged:false),(Value:0 ; Palette:1 ; Merged:false)),

 ((Value:2 ; Palette:3 ; Merged:false),(Value:4 ; Palette:2 ; Merged:false),
  (Value:0 ; Palette:2 ; Merged:false),(Value:16; Palette:2 ; Merged:false)),

 ((Value:2 ; Palette:2 ; Merged:false),(Value:4 ; Palette:3 ; Merged:false),
  (Value:0 ; Palette:3 ; Merged:false),(Value:0 ; Palette:3 ; Merged:false)),

 ((Value:2 ; Palette:4 ; Merged:false),(Value:0 ; Palette:4 ; Merged:false),
  (Value:8 ; Palette:4 ; Merged:false),(Value:16; Palette:4 ; Merged:false)));

  TestFieldFour : GameField=

(((Value:2 ; Palette:1 ; Merged:false),(Value:0 ; Palette:1 ; Merged:false),
  (Value:8 ; Palette:2 ; Merged:false),(Value:16; Palette:1 ; Merged:false)),

 ((Value:2 ; Palette:0 ; Merged:false),(Value:4 ; Palette:2 ; Merged:false),
  (Value:0 ; Palette:2 ; Merged:false),(Value:0 ; Palette:0 ; Merged:false)),

 ((Value:2 ; Palette:3 ; Merged:false),(Value:4 ; Palette:3 ; Merged:false),
  (Value:0 ; Palette:3 ; Merged:false),(Value:16; Palette:3 ; Merged:false)),

 ((Value:4 ; Palette:4 ; Merged:false),(Value:4 ; Palette:4 ; Merged:false),
  (Value:8 ; Palette:4 ; Merged:false),(Value:0 ; Palette:4 ; Merged:false)));


FieldOneRight : RightPattern =

 ((0,2,4,4),(0,0,4,8),(0,0,0,16),(0,0,0,32));

FieldTwoLeft  : RightPattern =

 ((4,4,2,0),(8,4,0,0),(16,0,0,0),(32,0,0,0));

FieldThreeUp  : RightPattern =

 ((4,8,16,32),(4,4,0,0),(2,0,0,0),(0,0,0,0));

FieldFourDown : RightPattern =

 ((0,0,0,0),(2,0,0,0),(4,4,0,0),(4,8,16,32));

ShiftSum  =  4 + 8 + 16 + 32 ;
TestLeftTop : Point=(Col : 1;Row : 2);

{ --- --- --- }

Procedure WriteTestResults(var A:TestResultList);
var
  i:Word;
begin
  TextColor(Yellow);
  Write('Results : ');
  TextColor(Green);
  For i:=1 to TestQuantity do
  begin
    Write('[ ');
    if A[i]=true then
      Write('*')
    else
      Write(' ');
    Write(' ]');
  end;
  ResetTerminal;
  WriteLn;
end;

{ --- }

procedure WriteFields(var A:GameField; var B:RightPattern);
var
  pos_h,pos_v:Word;
begin
  for pos_v:=0 to FieldSize do
  begin
    Write('|');
    for pos_h:=0 to FieldSize do
      Write(A[pos_v,pos_h].Value:2,'|');
    Write(' -->  |');
    for pos_h:=0 to FieldSize do
      Write(B[pos_v,pos_h]:2,'|');
    WriteLn;
  end;
end;

{ --- }

function CheckResultPassed(var A:GameField; var B:RightPattern):boolean;
var
  pos_h,pos_v:Word;
  tmp:Boolean;
begin
  tmp:=true;
  for pos_v:=0 to FieldSize do
    begin
      for pos_h:=0 to FieldSize do
        if A[pos_v,pos_h].Value <> B[pos_v,pos_h] then
        begin
          tmp:=false;
          break;
        end;
      if tmp=false then
        break;
    end;
  CheckResultPassed:=tmp;
end;

{ --- }

Procedure EndTest
          (var TestField:GameField ; var CheckTable:RightPattern;
           ShiftRes:Word ; var ResList:TestResultList; var NumTest:word);
begin
  if CheckResultPassed(TestField,CheckTable) and
     (ShiftRes = ShiftSum) then
  begin
      TextColor(Green);
      WriteLn(' Passed !');
      ResList[NumTest]:=true;
  end else
  begin
    TextColor(Red);
    WriteLn(' Failed...');
  end;
  Delay(ViewPause);
  ResetTerminal;
  inc(NumTest);
end;

{ === === === }

VAR
  ShiftResult:Word=0;
  NumTest:Word=1;
  MainResult:TestResultList= (false,false,false,false);

BEGIN

{ Right shift }

  ClrScr;
  ShiftResult:=0;
  WriteLn('game2048 SHIFT2048.PAS test - RIGHT SHIFT');
  WriteFields(TestFieldOne,FieldOneRight);
  ShiftFieldRight(TestFieldOne,TestLeftTop,ShiftResult);
  Write(ShiftResult, ' => ',ShiftSum);
  EndTest(TestFieldOne,FieldOneRight,ShiftSum,MainResult,NumTest);
{ left shift }

  ClrScr;
  ShiftResult:=0;
  WriteLn('game2048 SHIFT2048.PAS test - LEFT SHIFT');
  WriteFields(TestFieldTwo,FieldTwoLeft);
  ShiftFieldLeft(TestFieldTwo,TestLeftTop,ShiftResult);
  Write(ShiftResult, ' => ',ShiftSum);
  EndTest(TestFieldTwo,FieldTwoLeft,ShiftSum,MainResult,NumTest);

{ top shift }

  ClrScr;
  ShiftResult:=0;
  WriteLn('game2048 SHIFT2048.PAS test - TOP SHIFT');
  WriteFields(TestFieldThree,FieldThreeUp);
  ShiftFieldUp(TestFieldThree,TestLeftTop,ShiftResult);
  Write(ShiftResult, ' => ',ShiftSum);
  EndTest(TestFieldThree,FieldThreeUp,ShiftSum,MainResult,NumTest);

{ lower shift }

  ClrScr;
  ShiftResult:=0;
  WriteLn('game2048 SHIFT2048.PAS test - LOWER SHIFT');
  WriteFields(TestFieldFour,FieldFourDown);
  ShiftFieldDown(TestFieldFour,TestLeftTop,ShiftResult);
  Write(ShiftResult, ' => ',ShiftSum);
  EndTest(TestFieldFour,FieldFourDown,ShiftSum,MainResult,NumTest);

{ write results }

  WriteTestResults(MainResult);

END.
