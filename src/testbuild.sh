#!/bin/bash

# SHIFT2048.PAS test build script

source libbuild.sh

echo -e "\n\e[35mSHIFT2048.PAS test build...\n\e[0m"

fpc -g -dBUILD_FOR_TEST shift2048.pas
check_compile_results $?
echo $?
if [ $? != 0 ]; then
  exit 1
fi
fpc -g -ot_shift t_shift.pas
check_compile_results $?
if [ $? -eq 0 ]; then
  echo -e "\e[1;34mPress \e[1;33m - Y - \e[1;34m to \e[1;31mRUN"
  echo -e "\e[1;34mPress \e[1;33many key\e[1;34m to \e[1;31mEXIT\e[0m"
  read -n 1 -s any_key
  case $any_key in
    [yY]*)
      ./t_shift
    ;;
    *)
      echo "Exit..." 
    esac
fi
exit 0