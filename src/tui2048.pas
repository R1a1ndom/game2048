{ --- MENU2048.PAS ---

  --- 2 0 4 8 ---

 console text-mode puzzle game
 text user-interface elements unit

}

UNIT TUI2048;


INTERFACE


Uses
  CRT,CRT_Base,Unit2048;

CONST

  MenuStart:Point=(Col:1;Row:1);
  MenuMax = 2;

  MsgMax      = 25;
  MenuItemMax = 25;
  HintLineMax = 77;

  MsgBoxHeight = 6;
  MsgBoxWidth  = 35;
  ButtonsVerticalOffset=4;
  
  ButtonsGap=4;
  ButtonEdge=2;

TYPE

  MsgString=string[MsgMax];
  MenuItemString=string[MenuItemMax];
  HintString=string[HintLineMax];

  ActionItem=record
    Caption:MenuItemString;
    Action:ProgramState
  end;

  ButtonPalette=record
    BGSelColor,BGUnselColor,SelColor,UnselColor:Word
  end;

CONST

{ Colors }
  GameMenuPalette:ButtonPalette=
  (BGSelColor : White  ; BGUnselColor : White ;
   SelColor   : Yellow ; UnselColor   : Black);

  BoxButtonsPalette:ButtonPalette=
  (BGSelColor  : Blue  ; BGUnselColor : Cyan ;
   SelColor : Yellow ; UnselColor   : Yellow);

  col_ClearMenu = LightGray;
  col_HintText  = Magenta;
  col_HintBG    = LightGray;
  col_BGMsgBox  = Cyan;
  col_Msg       = Yellow;
  col_DBGPrint  = Yellow;

{ button elements }
  buttLeftEdge  = '[ ';
  buttRightEdge = ' ]';
  buttEmpty     = '  ';

{ msg-boxes messages }
  NewGameMsg='Begin a new game ?';
  NoMovesMsg='Out of moves! Please choose actions';
  WinnerMsg ='Congratulations !!!';

  ConfirmCaption = '  O K  ';
  CancelCaption  = 'Cancel';
  NewGameCaption = 'New game';
  QuitCaption     = 'Quit';

{ hint lines }
  hintline_Main=
  'ARROWS - shift game field | BACKSPACE - cancel last move | ESC - game menu';
  hintline_PauseMenu=
  'LEFT / RIGHT - select | ENTER - choose';

  menuGameInfo=
  '<< 2 0 4 8 >>  text-mode puzzle game';
{$ifdef DEBUG}
  menuDebugInfo=
  '<< 2 0 4 8 >>  text-mode puzzle game - DEBUG version';
{$endif}

{ game menu }
  GameMenuList:array[0..MenuMax] of ActionItem =
  ((Caption:'Begin new game' ; Action:stNewGame),
   (Caption:'Show About/Help Window';Action:stShowAbout),
   (Caption:'Quit from 2048' ; Action:stQuitGame));

{ no-moves msgbox buttons }
  OutOfMovesList:array[0..MenuMax] of ActionItem =
  ((Caption:'New game' ; Action:stNewGame),
   (Caption:'Undo' ; Action:stUndoLast),
   (Caption:'Quit' ; Action:stQuitGame));

{ PROCEDURES and FUNCTIONS }


{$ifdef DEBUG}
Procedure ShowDebugInfo(NumOfIterations:Word;FreeCells:Word);
{$else}
Procedure ShowHintLine(Line:HintString);
{$endif}

procedure WriteButton
     (Caption:MenuItemString;Palette:ButtonPalette;Selected:Boolean);

Procedure ClearMenuArea;

function EvalMsgPos(Msg:MsgString;LeftTop:Point):Point;

Function GameMenu:ProgramState;

Function NewGameBox:ProgramState;

Function NoMovesMsgBox:ProgramState;

Function WinnerMsgBox:ProgramState;

Function AboutHelpWindow:ProgramState;


IMPLEMENTATION


{ --- }

{$ifdef DEBUG}
Procedure ShowDebugInfo(NumOfIterations:Word;FreeCells:Word);
begin
  TextBackground(col_HintBG);
  TextColor(col_DBGPrint);
  GotoXY(1,ScreenHeight);
  Write(#32,'Iterations : ',NumOfIterations,' Free Cells : ',FreeCells);
  ClearLine(ScreenWidth-WhereX);
end;
{$else}
Procedure ShowHintLine(Line:HintString);
begin
  TextBackground(col_HintBG);
  TextColor(col_HintText);
  GotoXY(1,ScreenHeight);
  Write(#32,Line);
  ClearLine(ScreenWidth-WhereX);
end;
{$endif}

{ --- }

procedure WriteButton
     (Caption:MenuItemString;Palette:ButtonPalette;Selected:Boolean);
begin
  if Selected then begin
    TextBackground(Palette.BGSelColor);
    TextColor(Palette.SelColor);
    Write(buttLeftEdge);
  end else begin
    TextBackground(Palette.BGUnselColor);
    TextColor(Palette.UnselColor);
    Write(buttEmpty);
  end;
  Write(Caption);
  if Selected then
    Write(buttRightEdge)
  else
    Write(buttEmpty);
end;

{ --- }

function EvalMsgPos(Msg:MsgString;LeftTop:Point):Point;
var
  tmp:Point;
begin
  tmp.Row:=LeftTop.Row+2;
  tmp.Col:=LeftTop.Col+ ( MsgBoxWidth - Length(Msg) ) div 2;
  EvalMsgPos:=tmp;
end;

{ --- }

Procedure ClearMenuArea;
begin
  TextBackground(col_ClearMenu);
  TextColor(col_HintText);
  GotoXY(MenuStart.Col,MenuStart.Row);
{$ifdef DEBUG}
  Write(#32#32,menuDebugInfo);
{$else}
  Write(#32#32,menuGameInfo);
{$endif}
  ClearLine(ScreenWidth-WhereX);
end;

{ --- }

Function GameMenu:ProgramState;

  procedure WriteFullMenu(Start:Point;Pos:Word);
  var
    i:Word;
  begin
    TextBackground(col_ClearMenu);
    GotoXY(Start.Col,Start.Row);
    Write(#32#32#32);
    if Pos>0 then
    For i:=0 to Pos-1 do
      WriteButton(GameMenuList[i].Caption,GameMenuPalette,false);
    WriteButton(GameMenuList[Pos].Caption,GameMenuPalette,true);
    if Pos<MenuMax then
      For i:=Pos+1 to MenuMax do
        WriteButton(GameMenuList[i].Caption,GameMenuPalette,false);
    ClearLine(ScreenWidth-WhereX);
  end;

var
  sym:Char;
  MenuPos:Word;
begin
  MenuPos:=0;
  WriteFullMenu(MenuStart,MenuPos);
  repeat
    sym:=ReadKey;
    case sym of
      kbdRight: if MenuPos<MenuMax then {LEFT key - move cursor to the end}
      begin
        inc(MenuPos);
        WriteFullMenu(MenuStart,MenuPos);
      end;
      kbdLeft: if MenuPos>0 then   {RIGHT key - move cursor to the begin}
      begin
        dec(MenuPos);
        WriteFullMenu(MenuStart,MenuPos);
      end
    end;
  until (sym=kbdENTER) or (sym=kbdESC);
  ClearMenuArea;
  if sym=kbdENTER then
    GameMenu:=GameMenuList[MenuPos].Action { ENTER - menu result }
  else
    GameMenu:=stContinue;  { ESC - continue }
end;

{ --- }

Function NewGameBox:ProgramState;

  function EvalButtonsPos(LeftTop:Point):Point;
  var
    tmp:Point;
  begin
    tmp.Row:=LeftTop.Row+ButtonsVerticalOffset;
    tmp.Col:=MiddleCol-
       (Length(ConfirmCaption+CancelCaption)+
        ButtonEdge*4+ButtonsGap) div 2;
    EvalButtonsPos:=tmp;
  end;

  function DrawNewGameBox(Msg:String):Point;
  var
    LeftTop,MsgPos:Point;
  begin
    Textbackground(col_BGMsgBox);
    TextColor(col_Msg);
    LeftTop:=ClearRectInMid(MsgBoxWidth,MsgBoxHeight);
    MsgPos:=EvalMsgPos(NewGameMsg,LeftTop);
    GotoXY(MsgPos.Col,MsgPos.Row);
    Write(Msg);
    DrawNewGameBox:=LeftTop;
  end;

  procedure DrawButtons(Pos:Word;StartPos:Point);
  begin
    GotoXY(StartPos.COl,StartPos.Row);
    if Pos=0 then
      WriteButton(ConfirmCaption,BoxButtonsPalette,true)
    else
      WriteButton(ConfirmCaption,BoxButtonsPalette,false);
    GotoXY(WhereX+ButtonsGap,WhereY);
    if Pos=0 then
      WriteButton(CancelCaption,BoxButtonsPalette,false)
    else
      WriteButton(CancelCaption,BoxButtonsPalette,true);
    CursorOut;
  end;

var
  NewGameLeftTop,ButtonsPos:Point;
  sym:Char;
  Pos:Word;
begin
  Pos:=0;
  NewGameLeftTop:=DrawNewGameBox(NewGameMsg);
  ButtonsPos:=EvalButtonsPos(NewGameLeftTop);
  DrawButtons(Pos,ButtonsPos);
  repeat
    sym:=ReadKey;
    if (sym=kbdLeft) or (sym=kbdRight) then
    begin
      if Pos=0 then
        Pos:=1
      else
        Pos:=0;
      DrawButtons(Pos,ButtonsPos);
    end;
  until (sym=kbdENTER) or (sym=kbdESC);
  TextBackground(col_BGMain);
  NewGameLeftTop:=ClearRectInMid(MsgBoxWidth,MsgBoxHeight);
  if (sym=kbdENTER) and (Pos=0) then
    NewGameBox:=stNewGame
  else
    NewGameBox:=stContinue;
end;

{ --- }

Function NoMovesMsgBox:ProgramState;


begin


  NoMovesMsgBox:=stContinue;


end;

{ --- }

Function WinnerMsgBox:ProgramState;


begin


  WinnerMsgBox:=stContinue;


end;

{ --- }

Function AboutHelpWindow:ProgramState;


begin


  AboutHelpWindow:=stContinue;


end;


{ --- === --- === --- }

END.