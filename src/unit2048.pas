{
  --- UNIT2048.PAS ---

  2048 text-mode puzzle game
}

UNIT Unit2048;


INTERFACE

USES
  CRT,CRT_Base;

CONST

{ title picture size }

TitlePictureHeight = 7;
TitlePictureWidth  = 34;

FieldSize =  3; { 0..3 }
MaxCells  = 16; { field 4 x 4 }


WinNumber=2048; { fix for debug }

{colors}

col_TitleMain  = Green;
col_TitleMsg   = Yellow;
col_BGMain     = Black;
col_Grid       = White;

{delays}

PictureDrawPause=100;
MessageDrawPause=200;
{$ifdef DEBUG}
DebugDelay = 100;
{$endif}

TYPE

{ title screen picture }

AsciiPicture=array[1..TitlePictureHeight] of String[TitlePictureWidth];

{ game field types }

CellPalette=record
  BGColor,NumColor:Word
end;

{ game field eilement }

Cell=record
  Value:LongWord;    { cell's value }
  Palette:Word;      { number of palette }
  Merged:Boolean;     { already merged in this move }
end;

{ game field array }

GameField=array[0..3,0..3] of Cell;

UndoData=record
  Field:GameField;
  Score,HiScore:LongWord
end;

{ program state }

ProgramState =
( stNewGame,       { - begin new game }
  stContinue,      { - continue game }
  stContinueRedraw,{ - continue game, full redraw game field }
  stShowAbout,     { - show About/Help window }
  stWinner,        { - win situation (gamer become 2048 number)}
  stNoMoves,       { - no moves situation }
  stConfirm,       { - confirm action from MsgBox() }
  stCancel,        { - cancel action from MsgBox() }
  stUndoLast,      { - undo last move }
  stQuitGame       { - quit }
);

CONST

MainTitle:ASCIIPicture =
(' *****    *****   **   **   ***** ',
 '**   **  **   **  **   **  **   **',
 '    **   **   **  **   **  **   **',
 '  **     **   **  *******   ***** ',
 ' **      **   **       **  **   **',
 '**   **  **   **       **  **   **',
 '*******   *****        **   ***** ');

TitleAnyKeyMessage=
'< Press any key ... >';


CellWidth  = 6;
CellHeight = 3;

FieldHeight = CellHeight * (FieldSize+1) + 3 ;
FieldWidth  = CellWidth  * (FieldSize+1) + 3 ;


PaletteTable:array[0..11] of CellPalette=(

(BGColor:black      ; NumColor:LightGray),  { empty }
(BGColor:blue       ; NumColor:LightGray),  { 2 }
(BGColor:Green      ; NumColor:yellow),     { 4 }
(BGColor:Red        ; NumColor:LightGray),  { 8 }
(BGColor:Yellow     ; NumColor:Red),        { 16 }
(BGColor:Magenta    ; NumColor:LightGray),  { 32 }
(BGColor:Cyan       ; NumColor:blue),       { 64 }
(BGColor:White      ; NumColor:magenta),    { 128 }
(BGColor:White      ; NumColor:Red),        { 256 }
(BGColor:White      ; NumColor:Green),      { 512 }
(BGColor:Black      ; NumColor:Yellow),     { 1024 }
(BGColor:Black      ; NumColor:red));       { 2048 }


{
 --------------------------
  Procedures and functions
 --------------------------
}

Procedure ShowTitleScreen;

Procedure DrawGrid;

Procedure ResetCell(var A:GameField;VPos,HPos:Word);

Procedure ClearField(var A:GameField);

Procedure ClearUndoData(var A:UndoData);

Function EvalCellLeftTop(LeftTop:Point;VPos,HPos:Word):Point;

Procedure DrawOneCell(var A:GameField;VPos,HPos:Word;LeftTop:Point);

function GetNewNumber:Word;

{$ifdef DEBUG}
procedure AddNewNumber(var A:GameField;var NumOfIteration:Word);
{$else}
procedure AddNewNumber(var A:GameField);
{$endif}

Procedure WriteScore(LT:Point; Sc,HiSc:LongWord);

Procedure DrawAllField(LeftTop:Point;A:GameField);


IMPLEMENTATION


CONST

{ numbers for RANDOM - for new numbers generation }

  NumGenerateMax = 99;
  NumGenerateTrim = 9;

{ relative coords for SCORE and RECORD print}

  ScoreMsgCol = 0;    { Message }
  ScoreMSgRow = -2;

  ScoreNumCol = 0;    { Number }
  ScoreNumRow = -1;

  RecordMsgCol = 21;  { Message }
  RecordMsgRow = -2;

  RecordNumCol = 21;   { Number }
  RecordNumRow = -1;

Procedure ShowTitleScreen;

  Procedure WriteTitlePicture;
  var
    Title:Point;
    i:Word;
  begin
    Title.Col:=WhereX;
    Title.Row:=WhereY;
    for i:=1 to TitlePictureHeight do
    begin
      GotoXY(Title.Col,Title.Row);
      Write(MainTitle[i]);
      CursorOut;
      inc(Title.Row);
      Delay(PictureDrawPause);
    end;
  end;

var
  LeftTop:Point;
begin
  LeftTop:=GetScreenMiddle;
  LeftTop.Col:=LeftTop.Col-(TitlePictureWidth div 2);
  LeftTop.Row:=LeftTop.Row-TitlePictureHeight;
  ClrScr;
  TextColor(col_TitleMain);
  GotoXY(LeftTop.Col,LeftTop.Row);
  WriteTitlePicture;
  Delay(MessageDrawPause);
  LeftTop.Col:=(ScreenWidth - Length(TitleAnyKeyMessage)) div 2;
  LeftTop.Row:=(ScreenHeight div 4)*3;
  TextColor(col_TitleMsg);
  GotoXY(LeftTop.Col,LeftTop.Row);
  Write(TitleAnyKeyMessage);
  CursorOut;
end;

{ --- }

Procedure DrawGrid;

const
  GridTile  = '+';
  ScoreStr  = 'SCORE  :';
  RecordStr = 'RECORD :';

  Procedure DrawGridString;
  var
    i:Word;
  begin
    Write(GridTile);
    for i:=0 to FieldSize do
    begin
      GotoXY(WhereX+CellWidth,WhereY);
      Write(GridTile);
    end;
  end;

  Procedure WriteScoreString(GridBegin:Point);
  begin
    GotoXY(GridBegin.Col+ScoreMsgCol,GridBegin.Row+ScoreMsgRow);
    Write(ScoreStr);
    GotoXY(GridBegin.Col+RecordMsgCol,GridBegin.Row+RecordMsgRow);
    Write(RecordStr);
  end;

var
  GridBegin:Point;
  i:Word;
begin
  TextBackground(col_BGMain);
  TextColor(col_Grid);
  GridBegin:=GetMidRectLeftTop(FieldWidth+2,FieldHeight+2);
  WriteScoreString(GridBegin);
  for i:=0 to FieldSize+1 do
  begin
    GotoXY(GridBegin.Col,GridBegin.Row);
    DrawGridString;
    GridBegin.Row:=GridBegin.Row+CellHeight+1;
  end;
end;

{ --- }

Procedure ResetCell(var A:GameField;VPos,HPos:Word);
Begin
  with A[VPos,HPos] do
  begin
    Value:=0;
    Palette:=0;
    Merged:=false;
  end;
end;

{ --- }

Procedure ClearField(var A:GameField);
var
  i,j:Word;
begin
  for i:=0 to FieldSize do
    for j:=0 to FieldSize do begin
      ResetCell(A,i,j);
    end;
end;

{ --- }

Procedure ClearUndoData(var A:UndoData);
begin
  ClearField(A.Field);
  A.Score:=0;
  A.HiScore:=0;
end;


{ --- }

Function EvalCellLeftTop(LeftTop:Point;VPos,HPos:Word):Point;
var
  tmp:Point;
begin
  tmp.Col:=LeftTop.Col+HPos*(CellWidth+1);
  tmp.Row:=LeftTop.Row+VPos*(CellHeight+1);
  EvalCellLeftTop:=tmp;
end;

{ --- }

Procedure DrawOneCell(var A:GameField;VPos,HPos:Word;LeftTop:Point);

  function EvalNumberPosition(CellLeftTop:Point;Number:Word):Point;
  var
    tmp:Point;
  begin
    tmp:=CellLeftTop;
    inc(tmp.Row);
    if Number <= 8 then
      tmp.Col:=tmp.Col + 3
    else if Number <= 512 then
      tmp.Col:=tmp.Col + 2
    else
      tmp.Col:=tmp.Col + 1;
    EvalNumberPosition:=tmp;
  end;

{ --- }

begin
  with PaletteTable[ A[VPos,HPos].Palette ] do begin
    TextBackground(BGColor);
    TextColor(NumColor);
  end;
  LeftTop:=EvalCellLeftTop(LeftTop,VPos,HPos);
  ClearRect(LeftTop,CellWidth,CellHeight);
  if A[VPos,HPos].value <> 0 then begin
    LeftTop:=EvalNumberPosition(LeftTop,A[VPos,HPos].value);
    GotoXY(LeftTop.Col,LeftTop.Row);
    Write(A[VPos,HPos].value);
  end;
end;

{ --- }

Procedure WriteScore(LT:Point; Sc,HiSc:LongWord);
begin
  GotoXY(LT.Col+ScoreNumCol,LT.Row+ScoreNumRow);
  Write(Sc);
  GotoXY(LT.Col+RecordNumCol,LT.Row+RecordNumRow);
  Write(HiSc);
end;

{ --- }

Procedure DrawAllField(LeftTop:Point ; A:GameField);
var
  pos_h,pos_v:Word;
begin
  for pos_h:=0 to FieldSize do
    for pos_v:=0 to FieldSize do begin
      DrawOneCell(A,pos_v,pos_h,LeftTop);
    end;
end;

{ --- }

function GetNewNumber:Word;
var
  c:Word;
begin
  c:=random(NumGenerateMax);
  if c<NumGenerateTrim then
    GetNewNumber:=4
  else
    GetNewNumber:=2
end;

{ --- }

{$ifdef DEBUG}
procedure AddNewNumber(var A:GameField;var NumOfIteration:Word);
{$else}
procedure AddNewNumber(var A:GameField);
{$endif}
var
  NewNum,HNum,VNum:Word;
begin
  NewNum:=GetNewNumber;
{$ifdef DEBUG}
  NumOfIteration:=0;
{$endif}
  repeat
    HNum:=Random(4);
    VNum:=Random(4);
{$ifdef DEBUG}
    inc(NumOfIteration);
{$endif}
  until A[HNum,VNum].Value=0;
  A[HNum,VNum].Value:=NewNum;
  if  NewNum=2  then
    A[HNum,VNum].Palette:=1
  else
    A[HNum,VNum].Palette:=2;
end;

{ --- ---  ===  ===  --- --- }

END.